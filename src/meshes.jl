import CairoMakie as MK

function vis(geodata::MeshData; variable, kwargs...)
    fig, ax, hm = viz(domain(geodata), color = geodata[:, variable]; kwargs...)
    if haskey(kwargs, :colorscheme)
        MK.Colorbar(fig[1, 2], colormap = kwargs[:colorscheme],
            limits = extrema(geodata[:, variable]))
    end
    fig
end

using PointPatterns

function vizpp(P::PoissonProcess{<:Function}, g::Union{Domain, Geometry}; palette = :RdBu)
    # auxiliary grid
    C = CartesianGrid(extrema(g)..., dims = (100, 100))
    G = view(C, g)

    fig = MK.Figure()
    # intensity and point pattern
    subfig = fig[1,1] = MK.GridLayout()
    ## intensity
    MK.Axis(subfig[1,1], title =  "Intensity function")
    viz!(G, color = P.λ.(centroid.(G)), colorscheme = palette)
    viz!(g, showfacets = true, color = :white, alpha = 0)
    ## point pattern
    ppaxis = MK.Axis(subfig[1,2], title = "Spatial point pattern")
    viz!(g, showfacets = true, color = :white, alpha = 0)
    viz!(rand(P, g), color = :black, pointsize = 5)
    MK.hideydecorations!(ppaxis, grid = false)
    MK.colgap!(subfig, 5)
    # colorbar
    MK.Colorbar(fig[1,2], colormap = palette, limits = extrema(P.λ.(centroid.(G))))

    fig
end

function vizpp(P::PoissonProcess{<:Real}, g::Union{Domain, Geometry}; palette = :tomato)
    # auxiliary grid
    C = CartesianGrid(extrema(g)..., dims = (100, 100))
    G = view(C, g)

    fig = MK.Figure()
    # intensity and point pattern
    subfig = fig[1,1] = MK.GridLayout()
    ## intensity
    MK.Axis(subfig[1,1], title =  "Intensity function")
    viz!(G, color = palette)
    viz!(g, showfacets = true, color = :white, alpha = 0)
    ## point pattern
    ppaxis = MK.Axis(subfig[1,2], title = "Spatial point pattern")
    viz!(g, showfacets = true, color = :white, alpha = 0)
    viz!(rand(P, g), color = :black, pointsize = 5)
    MK.hideydecorations!(ppaxis, grid = false)
    MK.colgap!(subfig, 5)
    # colorbar
    MK.Colorbar(fig[1,2], colormap = [palette, palette], limits = (P.λ - 1, P.λ + 1),
        ticks = ([P.λ]))

    fig
end

function vizpp(P::PoissonProcess{<:AbstractVector}, g::Domain; palette = :RdBu, fun = identity)
    fig = MK.Figure()
    # intensity and point pattern
    subfig = fig[1,1] = MK.GridLayout()
    ## intensity
    MK.Axis(subfig[1,1], title =  "Intensity function")
    viz!(g, showfacets = true, color = fun.(P.λ), colorscheme = palette)
    ## point pattern
    ppaxis = MK.Axis(subfig[1,2], title = "Spatial point pattern")
    viz!(g, showfacets = true, color = :white, alpha = 0)
    viz!(rand(P, g), color = :black, pointsize = 5)
    MK.hideydecorations!(ppaxis, grid = false)
    MK.colgap!(subfig, 5)
    # colorbar
    MK.Colorbar(fig[1,2], colormap = palette, limits = extrema(fun.(P.λ)))

    fig
end

function vizpp(P::PoissonProcess{<:AbstractVector}, g::Domain, border::Geometry; palette = :RdBu, fun = identity)
    fig = MK.Figure()
    # intensity and point pattern
    subfig = fig[1,1] = MK.GridLayout()
    ## intensity
    MK.Axis(subfig[1,1], title =  "Intensity function")
    viz!(g, color = fun.(P.λ), colorscheme = palette)
    viz!(border, showfacets = true, color = :white, alpha = 0)
    ## point pattern
    ppaxis = MK.Axis(subfig[1,2], title = "Spatial point pattern")
    viz!(border, showfacets = true, color = :white, alpha = 0)
    viz!(rand(P, g), color = :black, pointsize = 5)
    MK.hideydecorations!(ppaxis, grid = false)
    MK.colgap!(subfig, 5)
    # colorbar
    MK.Colorbar(fig[1,2], colormap = palette, limits = extrema(fun.(P.λ)))

    fig
end

